EAPI="5"
PYTHON_COMPAT=( python3_{4,5,6} ) #test pypy

inherit distutils-r1

DESCRIPTION="CLI access to KIT-SCC's DNS WebAPI."
HOMEPAGE="https://gitlab.com/FloRinke/dnscli"
LICENSE="AGPL"

SLOT="0"
DEPEND="${PYTHON_DEPS} dev-python/python-distutils-extra[${PYTHON_USEDEP}] virtual/pkgconfig"

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="https://gitlab.com/FloRinke/dnscli.git"
	SRC_URI=""
else
	SRC_URI="https://FloRinke.com/FloRinke/dnscli/archive/v${PV}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

IUSE=""

RDEPEND="${DEPEND} net-libs/sccdnslib[${PYTHON_USEDEP}] dev-python/pygments[${PYTHON_USEDEP}] dev-python/tabulate[${PYTHON_USEDEP}] dev-python/click[${PYTHON_USEDEP}]"

src_compile() {
	distutils-r1_src_compile
}

src_install() {
	distutils-r1_src_install
}

