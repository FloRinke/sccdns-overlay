EAPI="5"
PYTHON_COMPAT=( python3_{4,5,6} ) #test pypy

inherit distutils-r1

DESCRIPTION="Python (3) library to access KIT SCC's WebAPI for DNS (and other network management)."
HOMEPAGE="https://gitlab.com/FloRinke/sccdnslib"
LICENSE="AGPL"

SLOT="0"
DEPEND="${PYTHON_DEPS} dev-python/python-distutils-extra[${PYTHON_USEDEP}] virtual/pkgconfig"

if [[ ${PV} == "9999" ]] ; then
	inherit git-2
	EGIT_REPO_URI="https://gitlab.com/FloRinke/sccdnslib.git"
	SRC_URI=""
else
	SRC_URI="https://gitlab.com/FloRinke/sccdnslib/archive/${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

IUSE=""

RDEPEND="${DEPEND} dev-python/requests[${PYTHON_USEDEP}] dev-python/pygments[${PYTHON_USEDEP}]"

src_compile() {
	distutils-r1_src_compile
}

src_install() {
	distutils-r1_src_install
}

